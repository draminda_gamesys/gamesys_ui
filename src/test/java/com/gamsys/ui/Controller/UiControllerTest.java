package com.gamsys.ui.Controller;

import com.gamsys.ui.controller.UiController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = UiController.class)
@AutoConfigureMockMvc
public class UiControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiControllerTest.class);
    @Autowired
    private MockMvc mvc;

    @Value("${content.cacheTime}")
    private Integer cacheTime;
    @Value("${content.dftPage}")
    private String dftPage;

    @Before
    public void setup(){
        LOGGER.debug("UiControllerTest.setup ");

    }
    @Test
    public void getContentsEmptyPageTest() throws Exception {
            LOGGER.debug("UiControllerTest.getContentsTest ");
        ResultActions resultActions = mvc.perform(
                MockMvcRequestBuilders.get("/content")
                .contentType(MediaType.TEXT_HTML)
        );
        MvcResult result =  resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        Assert.assertNotNull(result.getResponse().getHeader("Cache-Control"));
        Assert.assertEquals("max-age="+cacheTime+", must-revalidate, no-transform",result.getResponse().getHeader("Cache-Control"));
        Assert.assertEquals(dftPage,result.getModelAndView().getModel().get("pageRequest"));
    }

    @Test
    public void getContentsEmptyMainTest() throws Exception {
        LOGGER.debug("UiControllerTest.getContentsTest ");
        ResultActions resultActions = mvc.perform(
                MockMvcRequestBuilders.get("/content?contentName=main")
                        .contentType(MediaType.TEXT_HTML)
        );
        MvcResult result =  resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        Assert.assertEquals("main",result.getModelAndView().getModel().get("pageRequest"));
    }
    @Test
    public void getContentsPopUpTest() throws Exception {
        LOGGER.debug("UiControllerTest.getContentsTest ");
        ResultActions resultActions = mvc.perform(
                MockMvcRequestBuilders.get("/content?contentName=modalPopUp")
                        .contentType(MediaType.TEXT_HTML)
        );
        MvcResult result =  resultActions
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        Assert.assertEquals("modalPopUp",result.getModelAndView().getModel().get("pageRequest"));
    }

}