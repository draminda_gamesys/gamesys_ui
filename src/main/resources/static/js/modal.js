var loram = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac nulla mollis, elementum ex in, eleifend ex. Vestibulum ut eleifend";

//content
var modal = document.getElementById("modalPopup");
var page = document.getElementById("pageContent");
var shadow =  document.getElementById("shadow");
var contentAria = document.getElementById("contentAria");
//btn
var popUpOpen = document.getElementById("popUpOpen");
var btnClose = document.getElementById("btnClose");
var clear = document.getElementById("clear")
var numberOfWords =  document.getElementById("numberOfWords");

// pop Up Open
popUpOpen.onclick = function() {
  modal.className = "modal show";
  page.className = "page-content blur";
  shadow.className = "shadow show";
}
// Close button
btnClose.onclick = function() {
  var sts = confirm("Are you need to close this?");
  if(sts){
    modal.className = "modal hidden";
    page.className = "page-content";
    shadow.className = "shadow hidden";
  }
}

clear.onclick = function(event){
    contentAria.innerHTML = "";
}

function generateContent(isPTag){
    contentAria.className = "content-aria ";
    var value = numberOfWords.value;
    var htmlElement = isPTag?"<div class='row'>":"<p>";
    for(var i=0;i<value;i++){
        htmlElement += (isPTag?"<p>":"")+loram+(isPTag?"</p></div>":"")
    }
    htmlElement +=  isPTag?"":"</p>";
    contentAria.innerHTML = htmlElement
}
