package com.gamsys.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableCaching
public class UiApplication extends SpringBootServletInitializer {
	private static final Logger lOG = LoggerFactory.getLogger(UiApplication.class);
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(UiApplication.class, args);
		lOG.info("UiApplication is started {}",context.getEnvironment());
	}

}
