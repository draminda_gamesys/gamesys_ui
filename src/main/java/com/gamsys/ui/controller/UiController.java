package com.gamsys.ui.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Raminda
 * @apiNote Main REST api
 */
@Controller
@ConfigurationProperties
public class UiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiController.class);

    @Value("${content.dftPage}")
    private String dftPage;
    @Value("${content.cacheTime}")
    private Integer cacheTime;

    /**
     * @apiNote Get Pages
     * @param name content Name
     */
    @GetMapping(value = "/content")
    public String getContent(
            final @RequestParam(name = "contentName",required = false, defaultValue = "")String name,
            Model model,HttpServletResponse response) {
        String modalName=name.isEmpty()?dftPage:name;
        LOGGER.debug("UiController.getContent start name : {}",modalName);
        model.addAttribute("pageRequest",modalName);
        response.setHeader("Cache-Control","max-age="+cacheTime+", must-revalidate, no-transform");
        return modalName.trim() ;
    }
}
