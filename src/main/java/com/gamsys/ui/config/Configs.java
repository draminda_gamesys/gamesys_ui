package com.gamsys.ui.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author raminda
 * @apiNote  general configs
 */
@Configuration
@ComponentScan("com.gamesys.ui")
@ConfigurationProperties
public class Configs {
}
