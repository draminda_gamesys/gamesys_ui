# gamesysUI

### Deploy Steps
## hi
#### Test
* Test - `mvn clean test`
* Sonar - `mvn install verify sonar:sonar`
#### Install
`mvn install`

#### Deploy
* WAR  - `mvn clean package`


### Configs

##### In Application.properties

`content.cachePeriod`=`3600`
`content.dftPage`=`main`
`content.error404Page`=`404`
`content.cacheTime`=`84000`

### End Point

`<host>/content`\
`<host>/content?contentName=<pagename>`\


#### Reference
* <a>https://start.spring.io
* <a>https://spring.io
* <a>https://stackoverflow.com
* <a>docs.sonarqube.org
* <a>community.sonarsource.com
* <a>medium.com
* <a>codefresh.io
* <a>https://tomgregory.com
