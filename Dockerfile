FROM maven:3.8-openjdk-11-slim

COPY ./target/ui-0.0.1-SNAPSHOT app.war

ENV PORT 80
EXPOSE $PORT

ENTRYPOINT ["java","-jar","-Dserver.port=${PORT}" ,"/app.war"]
